﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Test21.Models;

namespace Test21.Controllers
{
    [Route("api/[controller]")]
    public class ParcelsController : Controller
    {
        ParcelContext db = new ParcelContext();


        // GET: api/values
        [HttpGet]
        public IEnumerable<Parcel> Get()
        {
            return db.Parcels;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public Parcel Get(int id)
        {
            var item = db.Parcels.FirstOrDefault(x => x.Id == id);
            if (item == null)
            {
                return null;
            }

            return item;
        }


     
        // POST api/values
        [HttpPost]
        public void Post(Parcel parcel)
        {
            if (!ModelState.IsValid)
            {
                Context.Response.StatusCode = 400;
            }


            if (parcel != null)
            {
                db.Parcels.Add(parcel);
                db.SaveChanges();

                Get(parcel.Id);
            }
        }

        // PUT api/values/5
        [HttpPut]
        public void Put(Parcel parcel)
        {
            if (!ModelState.IsValid)
            {
                Context.Response.StatusCode = 400;
            }

            //var original = from p in db.Parcels where p.Id == id select p;

            var result = db.Parcels.SingleOrDefault(b => b.Id == parcel.Id);
            if (result != null)
            {
                result = parcel;
                db.SaveChanges();
            }
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            var item = db.Parcels.FirstOrDefault(x => x.Id == id);
            db.Parcels.Remove(item);
            db.SaveChanges();
        }
    }
}
