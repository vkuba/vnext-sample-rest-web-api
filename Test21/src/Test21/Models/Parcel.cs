﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Test21.Models
{
    public class Parcel
    {
        [ScaffoldColumn(false)]
        public int Id { get; set; }

        [Required]
        public string SenderAdress { get; set; }

        [Required]
        public string ReciverAdress { get; set; }

        [Required]
        public string SenderName { get; set; }

        [Required]
        public string ReciverName { get; set; }

        public bool IsDelivered { get; set; }
    }
}

