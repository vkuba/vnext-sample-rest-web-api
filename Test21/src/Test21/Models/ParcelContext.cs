﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;

namespace Test21.Models
{

    public class ParcelContext : DbContext
    {
        public DbSet<Parcel> Parcels { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Parcel>().Key(a => a.Id);

            base.OnModelCreating(builder);
            

        }
       

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseInMemoryStore();
        }
    }
}